import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Account from '../views/Account.vue'
import About from '../views/About.vue'
import News from '../views/News.vue'
import profileEdit from '../views/profileEdit.vue'
import Products from '../views/Products.vue'
import Registration from '../views/Registration.vue'
import myPoint from '@/components/accountComponents/accountBlockComponents/myPoint'
import passedTest from '@/components/accountComponents/accountBlockComponents/passedTest'
import testBlock from '@/components/accountComponents/accountBlockComponents/testBlock'
import testDoctorBlock from '@/components/accountComponents/accountBlockComponents/testDoctorBlock'
import testResult from '@/components/accountComponents/accountBlockComponents/testResult'
import videoLesson from '@/components/accountComponents/accountBlockComponents/videoLesson'
import videoProsmotr from '@/components/accountComponents/accountBlockComponents/videoProsmotr'
import newsInnerComponent from '@/components/newsComponent/newsInnerComponent'
import RegistrationTest from '@/components/registerTest/registerTest'
import aboutCompany from '@/components/aboutComponents/aboutCompany'
import FAQ from '@/views/FAQ'
import farmanadzor from '@/views/farmanadzor'
import accountCyclePage from '@/components/accountComponents/accountCyclePage'
// import accountCycleInstruction from '@/components/accountComponents/accountCycle/accountCycleInstruction'
import accountCycleSertificate from '@/components/accountComponents/accountCycle/accountCycleSertificate'
import Anketa from '@/views/Anketa'

Vue.use(VueRouter)

function guardMyroute(to, from, next) {
  let isAuthenticated= false;
  if( localStorage.getItem('usertoken') )
   isAuthenticated = true;
  else
   isAuthenticated= false;
 if(isAuthenticated) {
   next(); // allow to enter route
  } else{
   next('/'); // go to '/login';
  }
 }

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    // beforeEnter: guardMyroute,
    meta: {title: 'Home'}
  },
  {
    path: '/profileEdit',
    component: profileEdit
  },
  {
    path: '/catalog',
    component: Products,
    props: true,
    children: [
      {
        path: '/catalog/:id',
        component: () => import('@/components/productComponent/productBlock'),
        props: true,
      },
      {
        path: '/catalogInner/:id',
        component: () => import('@/components/productComponent/productInnerComponent'),
        props: true,
      },
    ]
  },
  {
    path: '/about',
    component: About,
    children: [
      {
        path: '/about/:id',
        component: aboutCompany
      },
    ]
  },
  {
    path: '/account',
    name: 'Account',
    component: Account,
    beforeEnter: guardMyroute,
    meta: {title: 'Account'},
    children: [
      {
        path: '',
        component: passedTest
      },
      {
        path: 'mypoint',
        component: myPoint
      },
      {
        path: 'videoLesson',
        component: videoLesson
      },
      {
        path: 'videoProsmotr',
        component: videoProsmotr
      },
      {
        path: 'testblock',
        name: 'testblock',
        component: testBlock,
        props: true
      },
      {
        path: 'testDoctorBlock',
        name: 'testDoctorBlock',
        component: testDoctorBlock,
        props: true
      },
      {
        path: 'testResult',
        component: testResult
      }
    ]
  },
  {
    path: '/news',
    component: News,
  },
  {
    path: '/newsInner/:id', props: true,
    component: newsInnerComponent
  },
  {
    path: '/Registration',
    component: Registration,
  },
  {
    path: '/RegistrationTest',
    component: RegistrationTest
  },
  {
    path: '/Search/:keyword',
    name: 'Search',
    component: () => import('@/components/searchComponent/searchPage'),
    props: true
  },
  {
    path: '/FAQ',
    name: 'FAQ',
    component: FAQ
  },
  {
    path: '/farmanadzor',
    component: farmanadzor
  },
  {
    path: '/accountCyclePage',
    component: accountCyclePage,
    children: [
      // {
      //   path: '',
      //   component: accountCycleInstruction
      // },
      {
        path: 'accountCycleSertificate',
        component: accountCycleSertificate
      }
    ]
  },
  {
    path: '/Anketa',
    component: Anketa,
  }
]

const router = new VueRouter({
  mode: 'hash',
  routes
})

export default router
