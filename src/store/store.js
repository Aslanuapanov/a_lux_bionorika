import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
Vue.use(Vuex)
export default new Vuex.Store({
    state: {
        correctArr: [],
        accountIn: false,
        logo: '',
        transData: '',
        finish: false,
        user_id: '',
        url: 'https://api.phytoneering.expert/api/',
        profession_id: ''
    },

    actions: {
      async fetchLogo({commit, getters, dispatch}, url = this.state.url){
        let selectItem = localStorage.getItem('language')
        if(localStorage.getItem('language') ){
          let select = JSON.parse(selectItem)
          const res = await axios(`${url}users/logotype`, {headers: {'Content-Language' : select.title.toLowerCase()}})
          const logo = await res.data.data
          commit('updateLogo', logo)
        }else{
          const res = await axios(`${url}users/logotype`, {headers: {'Content-Language' : 'ru'}})
          const logo = await res.data.data
          commit('updateLogo', logo)
        }
      },
      async fetchTranslate({commit, getters, dispatch}, url = this.state.url){
        let selectItem = localStorage.getItem('language')
        if(localStorage.getItem('language') ){
          let select = JSON.parse(selectItem)
          const trans = await axios.get(`${url}users/translate`, {headers: {"Content-Language" : select.title.toLowerCase()}})
          const transData = await trans.data.data
          commit('updateTranslate', transData)
        }else{
          const trans = await axios.get(`${url}users/translate`, {headers: {"Content-Language" : 'ru'}})
          const transData = await trans.data.data
          commit('updateTranslate', transData)
        }
    },
  },
    mutations: {
      reset(state) {
        state.correctArr = [];
      },
      updateLogo(state, logo){
        state.logo = logo
      },
      updateTranslate(state, transData){
        state.transData = transData
      }
    }
})
