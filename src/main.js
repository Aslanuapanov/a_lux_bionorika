import Vue from 'vue'
import App from './App.vue'
import router from './router'
import VueAnalytics from 'vue-analytics'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import Slick from 'vue-slick';
import vWow from 'v-wow';
import VAnimateCss from 'v-animate-css';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css'
import VuePaginate from 'vue-paginate'
import Vuex from 'vuex';
import 'es6-promise/auto';
import store from './store/store'
import Vuelidate from 'vuelidate'
import VueTheMask from 'vue-the-mask'
import VueCountdown from '@chenfengyuan/vue-countdown';
import vSelect from 'vue-select'
import VueI18n from 'vue-i18n'
import i18n from './i18n'
import VueYoutube from 'vue-youtube'
import VueYandexMetrika from 'vue-yandex-metrika'

Vue.use(VueYoutube)
Vue.use(VueI18n)
Vue.component('v-select', vSelect)
Vue.use(VueTheMask)
Vue.component(VueCountdown.name, VueCountdown)
Vue.config.productionTip = false
Vue.use(vWow);
Vue.use(BootstrapVue)
Vue.use(VuePaginate)
Vue.use(IconsPlugin)
Vue.use(Slick)
Vue.use(VAnimateCss)
Vue.use(Vuex)
Vue.use(Vuelidate)
Vue.use(VueAnalytics, {
  id: 'G-V2E4LPTCDH',
  router,
  debug: {
    sendHitTask: true
  }
})

Vue.use(VueYandexMetrika, {
  id: 70454677,
  router,
  env: process.env.NODE_ENV
})

new Vue({
  router,
  i18n,
  store,

  render: h => h(App)
}).$mount('#app')
