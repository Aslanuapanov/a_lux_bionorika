module.exports = {
    // options...
    devServer: {
          proxy: 'http://api.phytoneering.expert/api/',
      },

    pluginOptions: {
      i18n: {
        locale: 'ru',
        fallbackLocale: 'ru',
        localeDir: 'locales',
        enableInSFC: true
      }
    }
}
